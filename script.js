function normalize(str) {
    return str.trim().toLowerCase().normalize("NFD").replace(/\p{Diacritic}/gu, "")
}

function contains(a, b) {
    return normalize(a).search(normalize(b)) >= 0
}