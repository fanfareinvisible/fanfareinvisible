---
layout: layout.njk
title: Accueil
---

**Parce que nous vivons une ère d’inquiétude et de tristesse, où la conscience
de la complexité du monde nous plonge dans l’impuissance, où le futur se révèle lourd de menaces,
nous décidons de mettre en place les conditions d’une résistance joyeuse, capables d’épaissir le
tissu social.**

Musiciens confirmés ou débutants, jeunes, vieux, ou entre les deux, animés par
l’envie de participer à un projet commun de résistance, de luttes, de fêtes, et d’échanges, en
solidarité musicale avec les mouvements sociaux, nous réveillons notre capacité à agir en
participant à « la fanfare invisible ».

En état d’alarme citoyenne, adeptes de la non-violence comme stratégie d’action,
nous effectuons des attentats musicaux, un terrorisme de la clef de sol, de la délinquance
acoustique et nous nous mettons à disposition du mouvement social et des associations de lutte.

La « fanfare invisible » c’est :

Un répertoire de morceaux issus du patrimoine et des luttes, accessible sur le site

Un soutien à l’apprentissage de ce répertoire, voire à la découverte de
l’instrument, par un accompagnement solidaire de musiciens confirmés lors des répétitions
hebdomadaires.

Des sections locales et régionales, adeptes des valeurs et principes du présent manifeste.
