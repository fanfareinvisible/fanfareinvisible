---
layout: layout.njk
title: Liens
---

- [DROIT AU LOGEMENT](http://www.droitaulogement.org/)
- [RESEAU EDUCATION SANS FRONTIERES](https://reseau-resf.fr/)
- [LIGUE DES DROITS DE L'HOMME](https://www.ldh-france.org/)
- [LES AMIES ET AMIS DE LA COMMUNE DE PARIS](https://www.commune1871.org/)
- [COLLECTIF TOUS MIGRANTS](https://tousmigrants.weebly.com/)
- [ATTAC](https://france.attac.org/)
- [PARIS LUTTES INFO](https://paris-luttes.info/)
- [COLLECTIF INTER-HOPITAUX](https://www.collectif-inter-hopitaux.org/)
- [REVOLUTION PERMANENTE](https://www.revolutionpermanente.fr/)
- [SUD RAIL](https://sudrail.fr/)
- [ASSEMBLEE DES ASSEMBLEE DES GILETS JAUNES](http://assembleedesassemblees.org/)
- [ZONES A DEFENDRE](https://zad.nadir.org/)
- [CITOYENS RESISTANTS D'HIER ET D'AUJOURD'HUI](http://www.citoyens-resistants.fr/)
- [MEDIAPART](https://www.mediapart.fr/)
- [PARIS REVOLUTIONNAIRE](https://www.parisrevolutionnaire.com/)
- [LA CIMADE](https://www.lacimade.org/)
- [COLLECTIF VIES VOLEES](https://www.facebook.com/collectif.viesvolees/)
- [LES COMPAGNONS DE LA NUIT](http://www.compagnonsdelanuit.com/)
- [TELE BOCAL](http://telebocal.org/)
- [CHEZ LES CROQUIGNARDS](https://chezlescroquignards.fr/)
- [LA COMPAGNIE JOLIE MOME](https://cie-joliemome.org/)
- [LA PAROLE ERRANTE](https://laparoleerrantedemain.org/)
- [L'A.E.R.I](https://www.facebook.com/profile.php?id=203050530198649&_rdr)
- [COLLECTIF 20EME SOLIDAIRE](https://www.facebook.com/20emeSolidaire/)
- [ACRIMED](https://www.acrimed.org/)
- [BASSINES NON MERCI](https://bassinesnonmerci.fr/)
- [LES SOULEVEMENTS DE LA TERRE](https://lessoulevementsdelaterre.org/)


LES FANFARES AMIES...

...en France

[Fanfare de la Redonne](https://fr-fr.facebook.com/FanfareRedonne) (Draguignan),
[La Bérézina](https://www.lesnouveauxtroubadours.fr/buissons/la-berezina/) (Saint-Sever-du-Moustier),
[La Fanfare Invisible Bretagne](https://fibzh.org/) (Bretagne),
[La Fanfare Invisible de Brest](https://fibzh.org/2019/12/06/la-fanfare-invisible-a-brest/) (Brest),
[FAME](https://fame.nry.pw/) (Lyon),
[La Fanfare de Lutte](https://philipperevelli.com/lille-de-lutte-fanfares-et-1er-mai/) (Lille),
[La Fanfare-Chorale de Lutte](https://www.le-tamis.info/structure/fanfare-chorale-de-lutte) (Grenoble),
[Kapital Gros Bruit](https://www.facebook.com/KapitalGrosBruit/) (Nantes),
[La ManiFanfare](https://www.facebook.com/La-ManiFanfare-110164930463466/?ref=page_internal) (Toulouse),
[Louise Michel Cacophonie](https://mars-infos.org/la-louise-michel-cacophonie-4749) (Marseille),
[La Fanfare de la Clé de Lutte](https://fanfare-cledelutte.fr/) (Paris),
[La grâce de l'Hippopotame](https://www.facebook.com/lagracedelhippo/) (Paris),
[Dans la Gueule du Loup](https://www.facebook.com/FanfareDansLaGueuleDuLoup/) (Ménilles),
[La Fanfoire](https://fr-fr.facebook.com/Fanfoire/) (Paimpol),
[Marcel Frontale](https://www.facebook.com/MarcelFrontale) (Lyon), 
[Collectif Fanfare de Briançon](https://chezlescroquignards.fr/les-collectifs/collectif-fanfare/) (Briançon)

...en Italie

[Ottoni a scoppio](http://www.ottoniascoppio.org/) (Milan),
[La FONC](https://www.facebook.com/laFONC/) (Milan),
[Fiati Sprecati](http://fiatisprecati.org/) (Florence),
[Titubanda](http://www.titubanda.it/it/) (Rome),
[Banda Roncati](https://fr-fr.facebook.com/profile.php?id=100027924685066) (Bologne)

...en Belgique

[La clinik du Dr Poembak](http://poembak.be/) (Bruxelles),
[KermesZ à l’est](https://kermeszalest.com/en) (Bruxelles)

...aux Pays-Bas

[Fanfare van de Eerste Liefdesnacht](http://www.liefdesnacht.nl/) (Amsterdam),
[Toeters en Bellen](https://www.toeters-en-bellen.nl/html/tb5_home.html) (Amsterdam)

...en Autriche

[Street Noise Orchestra](http://www.streetnoise.at/) (Innsbruck),
[Massala Brass Kollektiv](http://www.masalabrass.org/) (Graz)

...en Espagne

[La Fanfarria Contratiempo](https://fanfarriacontratiempo.wordpress.com/) (Barcelone),
[La Charanga Ventolín](https://www.facebook.com/charanga.ventolin) (Asturies)

...en Allemagne

[Express Brass Band](https://www.expressbrassband.de/) (Munich),
[Brasskarawane](https://www.youtube.com/watch?v=3XfEXyet0rM) (Dresde),
[Beatprotest](https://www.facebook.com/beatprotest/) (Munich),
[Capella Rebella](https://vimeo.com/435104090/) (Stuttgart)

...en Suisse

[La Fanfare du Château](https://fanfareduchateau.ch/) (Genève),
[La Fanfare militante de Lausanne](https://www.leslents.ch/la-demeure/) (Lausanne),

...en Angletterre

[The Stroud Red Band](https://www.facebook.com/The-Stroud-Red-Band-1214331765300486/) (Londres)

...aux Etats-Unis

[Rude Mechanical Orchestra](http://rudemechanicalorchestra.org/) (New York),
[Musicians Action Group](https://musiciansactiongroup.org/) (San Francisco),
[Bread&Puppet Theatre](http://breadandpuppet.org/) (Vermont),
[Second Line Brass Band](https://secondlinebrassband.org/) (Boston)

...au Brésil

[La Fanfarra Clandestina](https://www.facebook.com/fanfarraclandestina/) (São Paulo)

...Au Canada

[Chaotic Insurrection Ensemble](https://chaoticinsurrectionensemble.org/) (Montréal)


