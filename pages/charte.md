---
layout: layout
title: Charte
---
# Une fanfare, un engagement…

## Appartenance

La *Fanfare Invisible* est un collectif musical de soutien au mouvement social. En manifestation ou dans tout autre événement, jouer de la musique est pour nous un acte de non violence active, envisagée et adoptée comme stratégie d’action. Nous nous réunissons autour d’un manifeste écrit collectivement, et présent sur notre site internet [lafanfareinvisible.fr](https://lafanfareinvisible.fr). Il met en mots ce pourquoi nous nous retrouvons chaque semaine en répétition et dans la rue. Chaque nouvelle personne arrivante se doit de l’avoir lu et d’y adhérer, sinon sa participation au sein de cette fanfare n’aurait pas de sens.

## Droits

À partir de là, notre principe est simple. Dès que tu arrives en répétition, que tu participes à une sortie, tu es membre à part entière de la Fanfare Invisible, au même titre que les autres, même les personnes qui sont là depuis longtemps. Tu as les mêmes droits, y compris celui d’ouvrir grand la bouche pour râler, ou pour proposer.

Chacune et chacun de nous est membre actif, au sens plein du terme. C’est difficile. C’est politique, dans le plus beau sens du mot. Et c’est une chose absolument magnifique.

## Pouvoir

Le pouvoir d’agir, de faire, de créer est collectif, la Fanfare Invisible croit beaucoup dans cette idée, qu’elle met en œuvre au quotidien. Inutile de chercher un meneur ou une meneuse parmi nous !

Chaque membre de la Fanfare essaye de faire avancer les choses. Le véritable secret est dans l’engagement de chacune et de chacun.

## Politique

Nous ne jouons dans aucun évènement organisé par un parti politique, mais nous rencontrons régulièrement des partis et des syndicats ancrés à gauche lors des actions, des manifestations et des sorties auxquelles nous participons. 

La Fanfare Invisible revendique son indépendance et la pluralité des opinions de chacun de ses membres, mais n’est pas une fanfare « apolitique », au contraire !

# … et une pratique musicale

## Niveau musical

La fanfare accueille des personnes débutant la musique, tout autant que des musiciens et des musiciennes avancé·e·s. Bien sûr, être musicien·ne signifie un minimum de travail personnel, pour s’épanouir et contribuer au collectif. Une trentaine de morceaux au moins au répertoire, c’est quand même du travail, pour chaque individualité. Chacun·e à son niveau.

## Répétitions

Les répétitions ont lieu le jeudi de 20h à 22h. Une présence régulière est conseillée, mais ne saurait être obligatoire. Pour les débutant·e·s ou fanfaron·ne·s qui souhaitent travailler un point précis du répertoire ou de l’instrument, la salle de répétition peut ouvrir à 19h00 (avant la répétition générale, mais il faut voir avec celles et ceux qui ont les clefs)

## Sorties

Il n’est pas nécessaire de connaître tous les morceaux du répertoire de la Fanfare Invisible pour participer aux sorties. Au début, trois notes et un peu de playback suffisent ! Les sorties sont un très bon moyen d’apprendre le répertoire « sur le tas 3 et surtout d’intégrer humainement le groupe. Après, ne pas jouer trop fort, ne pas déséquilibrer un ensemble par sa propre maladresse, mais aussi oser, essayer, c’est affaire de confiance, de responsabilité, de vigilance, d’écoute…

## Répertoire

Notre répertoire comprend avant tout des chants de lutte mais aussi des morceaux de styles différents. Nous tâchons de choisir des musiques et des arrangements variés et réalisables par une fanfare nombreuse, en respectant le niveau musical de chacun·e.

## Ressources

Un tableau accessible à tous et toutes, et dont le lien apparait en bas de chaque mail de la fanfare recense les différents morceaux dans l’onglet « Répertoire », avec plein d’infos : partoches, parfois des enregistrements, la personne référente…

Une liste des morceaux qui sortent très souvent peut également être communiquée. La vidéo du set de la fanfare à Honk est aussi un bon outil de travail car la prise de son est assez bonne et les morceaux sont des classiques du répertoire : <https://www.yiny.org/videos/watch/8e224fb2-34a8-4d23-9ff0-367ab48cf44e>

# Comment on s’organise, comment on décide ?

## Organisation et décisions en assemblée générale

Les assemblées générales ont lieu le 10 de chaque mois. Elles décident du choix des morceaux à intégrer au répertoire, de l’organisation ou la participation à des événements lointains dans le temps ou géographiquement, des choix de méthode quant au soutien à des mouvements militants, et d’autres points d’ordre général. Les décisions sont prises quand un accord est trouvé entre tous les membres présents, après débats.

## Organisation et décisions en répétition

Les décisions concernant les sollicitations ou les sorties en manif se prennent pendant la répétition du jeudi soir : on valide collectivement la pertinence de la participation de la fanfare à tel ou tel évènement.

Nous sommes très sollicités, les choix sont parfois difficiles.

Nous nous efforçons de prendre des décisions collectives basées sur la pertinence politique, stratégique, humaine de l’événement, et d’éviter les décisions du type « on va là parce que c’est là qu’il y a plus de monde disponible ».

## Inscriptions au tableau

Le compte se fait seulement dans un second temps. On est super nombreux, beaucoup ne viennent pas à chaque répétition, on a donc mis en place un tableau en ligne. La personne responsable de la sortie validée en répétition l’inscrit dans le tableau, et chacun·e peut s’inscrire. Une fois que suffisamment de fanfaron·ne·s se sont engagé·e·s et que tous les pupitres sont représentés, on valide définitivement la sortie.

Les débutant·e·s peuvent s’inscrire et indiquer qu’ils sont débutants et ne maîtrisent donc pas tous les morceaux. Nous en tenons compte dans l’organisation des pupitres.

## Paroles

Nous avons besoin de nous parler, vu notre nombre et la violence sociale à laquelle nous avons à faire face. Chacun.e des membres se prononce et nous tentons, patiemment et sincèrement, de faire en sorte que chaque parole soit respectée.

# Le savoir vivre

## Comportement

Nous sommes souvent invités, à Paris, en région parisienne, mais aussi dans beaucoup d’endroits en lutte, en France et à l’étranger. Il est essentiel pour nous d’être toujours respectueux des personnes qui nous accueillent. Si nous sommes joyeux et turbulents, le respect d’autrui nous tient à cœur. Musicalement et humainement. 

Chacune, chacun de nous est libre de ses actes, de ses rencontres, de ses propos, de ses consommations, pourvu que les règles fondamentales ici écrites demeurent respectées.

## Le choix de la non violence

Les propos et comportements violents nuisent à la qualité d’échange et à la convivialité que nous recherchons ; ils neutralisent les processus de transformation sociale. Pour nous qui sommes tou·te·s animé·e·s par l’envie de rencontre et l’ouverture aux autres, la bienveillance et le respect de chacun·e sont les conditions permanentes et nécessaires pour y accéder.

Nous sommes donc vigilants face à toute forme de violence, y compris celle que l’on peut s’infliger à soi-même, individuellement ou collectivement.

## Règles

Les propos sexistes, racistes, homophobes, discriminatoires... et autres formes d’insultes ne sont ni tolérables, ni tolérés. Nous adhérons tous à ce principe élémentaire. Toute personne qui ne le respecte pas s’expose à l’exclusion du groupe.

Nous l’avons fait. Exclure. Expérience difficile. Nous espérons que le groupe a su y faire face avec courage et lucidité. Nous espérons surtout ne pas avoir à le refaire. C’est extrêmement désagréable.

# AVEC LA POLICE

## Surveillance

Les échanges mails de la Fanfare Invisible sont régulièrement consultés par la police, il est important de tenir compte de cette information lors de l’envoi de mails sur la liste.

## Provocation

Nous défendons la dignité du mouvement social par une pratique festive de la musique. Le principe de non violence active guide notre comportement. En manifestation, nous tâchons donc de ne pas répondre aux nombreuses provocations policières. Nous jouons.

# Et ailleurs…

## D’autres Fanfares Invisibles

Dans plusieurs régions de France, des groupes musicaux reprennent notre répertoire et notre démarche de Fanfare de soutien au mouvement social. Et parfois même notre nom. Nous n’y voyons aucun inconvénient, au contraire, nous l’avons même souhaité et exprimé dans notre manifeste.

Si certains de ces groupes ne respectent pas nos principes festifs et non violents, nous ne pourrons que le déplorer, mais nous n’y pourrons rien : la *Fanfare Invisible* n’est pas une marque déposée, simplement un désir collectif en action. 

Mais jusqu’à présent, nous sommes émus de constater que ces démarches sont sœurs de la nôtre, car fondées sur l’engagement et le partage. Et puis, au fil du temps et des rencontres nous avons pu constater que nous n’avons rien inventé, et que la pratique musicale associée au mouvement social est une longue histoire.

Ces initiatives contribuent à l’émergence d’un grand « bloc de cuivres », en France et en Europe, constitué de musiciennes et de musiciens conscient·e·s que la musique peut défendre la dignité des luttes.