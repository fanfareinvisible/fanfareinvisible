const markdownIt = require("markdown-it");
const md = new markdownIt({ html: true, breaks: true });
module.exports = function (eleventyConfig) {
  eleventyConfig.addPassthroughCopy("image_menu");
  eleventyConfig.addPassthroughCopy("admin");
  eleventyConfig.addPassthroughCopy("favicon.ico");
  eleventyConfig.addPassthroughCopy("script.js");
  eleventyConfig.addFilter("markdown", (content) => {
    return md.renderInline(content);
  });
  return {
    dir: {
      input: "pages"
    }
  };
};
